package java3.mlz.model;

import java.util.List;

public abstract class AbsMoviesAndDetail {
	private List<Movie> movies;
	// movie detail
	private Movie movieDetail;

	/**
	 * method to show details of the movie
	 * 
	 * @param movie
	 * @return detail page
	 */
	public String show_detail(Movie movie) {
		movieDetail = movie;
		// stay on page
		return "";
	}

	/**
	 * back to the overview
	 * 
	 * @return Link
	 */
	public final String close() {
		movieDetail = null;
		return "";
	}

	/*
	 * getters and setters
	 */

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public Movie getMovieDetail() {
		return movieDetail;
	}

	public void setMovieDetail(Movie movie) {
		this.movieDetail = movie;
	}

	/**
	 * Dummy getter to get the size of the movie list.
	 * 
	 * @return
	 */
	public Integer getStoredMovies() {
		return movies.size();
	}
}
