package java3.mlz.model.managed;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import java3.mlz.business.Controller;
import java3.mlz.model.AbsMoviesAndDetail;

/**
 * Managed bean for search in OMDB
 * 
 * @author retoweber
 *
 */
@ManagedBean
@ViewScoped
public class MovieSearchBean extends AbsMoviesAndDetail {
	private String searchTitle;
	private Integer searchYear;

	/**
	 * event method to look up for omdb entries
	 * 
	 * @return "" stay on page
	 */
	public String search() {
		// close any displayed detail before executing new search.
		setMovieDetail(null);
		
		setMovies(Controller.searchInOMDB(searchTitle, searchYear));
		// stay on current page.
		return "";
	}

	/**
	 * event method to save found movie
	 * 
	 * @return targetPage
	 */
	public String save() {
		String targetPage = Controller.saveMovie(getMovieDetail());
		// re-initialise
		searchTitle = null;
		searchYear = null;
		setMovieDetail(null);
		return targetPage;
	}

	/**
	 * cancel method to return redirect string.
	 * 
	 * @return ""
	 */
	public String cancel() {
		return Controller.goToMyMovies();
	}

	/*
	 * getters and setters
	 */
	public String getSearchTitle() {
		return searchTitle;
	}

	public void setSearchTitle(String searchTitle) {
		this.searchTitle = searchTitle;
	}

	public Integer getSearchYear() {
		return searchYear;
	}

	public void setSearchYear(Integer searchYear) {
		this.searchYear = searchYear;
	}

}
