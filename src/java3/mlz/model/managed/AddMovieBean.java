package java3.mlz.model.managed;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import java3.mlz.business.Controller;
import java3.mlz.model.Movie;

/**
 * managed bean class to add a movie manually
 * 
 * @author retoweber
 *
 */
@ManagedBean
@ViewScoped
public class AddMovieBean {
	private Movie movie;

	/**
	 * managed bean method to save a movie
	 * 
	 * @param movie
	 * @return
	 */
	public String save() {
		return Controller.saveMovie(movie);
	}

	/**
	 * cancel method to return redirect string.
	 * 
	 * @return
	 */
	public String cancel() {
		return Controller.goToMyMovies();
	}

	/**
	 * Validate runtime with minutes
	 * 
	 * @param context
	 * @param comp
	 * @param value
	 * @throws ValidatorException
	 */
	public void validateRuntime(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
		if (value != null) {
			String strVal = (String) value;
			if (!strVal.isEmpty())
				// could be empty of should match
				if (!strVal.matches("^[0-9]{0,4} min$")) {
					throw new ValidatorException(new FacesMessage(message("runtime_enter_error")));
				}
		}
	}

	/**
	 * Validate poster URL
	 * 
	 * @param context
	 * @param comp
	 * @param value
	 * @throws ValidatorException
	 */
	public void validateUrl(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
		if (value != null) {
			String strVal = (String) value;
			if (!strVal.isEmpty())
				// could be empty of should match
				if (!strVal.matches(
						"^(http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$")) {
					throw new ValidatorException(
							new FacesMessage("Not a correct URL-Format. Example: http(s)://domain.org/filename.jpg"));
				}
		}
	}

	/*
	 * getters and setters
	 */
	public Movie getMovie() {
		// insure that a movie instance exists
		if (movie == null) {
			movie = new Movie();
		}
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	private String message(String key) {
		FacesContext fc = FacesContext.getCurrentInstance();
		ResourceBundle bundle = fc.getApplication().getResourceBundle(fc, "msg");
		return bundle.getString(key);
	}
}
