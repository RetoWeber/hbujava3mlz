package java3.mlz.model.managed;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import java3.mlz.business.Controller;
import java3.mlz.model.AbsMoviesAndDetail;

/**
 * Overview of my movies
 * 
 * @author retoweber
 *
 */
@ManagedBean
@ViewScoped
public class MyMoviesBean extends AbsMoviesAndDetail {

	/*
	 * constructor
	 */
	public MyMoviesBean() {
		setMovies(Controller.loadMovies());
	}

	/**
	 * delete entry
	 * 
	 * @return Link
	 */
	public final String delete() {
		Controller.deleteMovie(getMovieDetail());
		getMovies().remove(getMovieDetail());
		setMovieDetail(null);
		// stay on page
		return "";
	}

}
