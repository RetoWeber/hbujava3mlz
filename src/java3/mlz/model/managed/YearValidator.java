package java3.mlz.model.managed;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * call to validate short values as the year here in this project
 * 
 * @author retoweber
 *
 */
@FacesValidator
public class YearValidator implements Validator {

	@Override
	public void validate(FacesContext contect, UIComponent component, Object value) throws ValidatorException {
		if (value != null) {
			String strValue = String.valueOf(value);
			if (!strValue.isEmpty()) {
				// if not empty, try it to cast as a valid short number
				try {

					Short year = Short.valueOf(strValue);
					if (year < 1600 || year > 2100) {
						throwMessage();
					}

				} catch (Exception e) {
					throwMessage();
				}
			}
		}

	}

	/**
	 * message preparation. Validation starts in Shakepeare's time, even if there
	 * were no movies! But a great writer of plays with could be the base of a
	 * movie.
	 * 
	 * @throws ValidatorException
	 */
	private void throwMessage() throws ValidatorException {
		FacesContext fc = FacesContext.getCurrentInstance();
		ResourceBundle bundle = fc.getApplication().getResourceBundle(fc, "msg");
		throw new ValidatorException(new FacesMessage(bundle.getString("not_a_number_between1600and2100")));
	}

}
