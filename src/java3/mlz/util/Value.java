package java3.mlz.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class to work with values for checking and parsing.
 * 
 * @author retoweber
 *
 */
public final class Value {
	public static final String DATE_PATTERN_XML = "dd MMM yyyy";
	public static final Locale LOCALE_XML = new Locale("en", "US");

	/**
	 * checks if the string value is empty
	 * 
	 * @param value
	 * @return boolean
	 */
	public static final boolean isEmpty(String value) {
		if (value == null) {
			return true;
		}
		return value.isEmpty();
	}

	/**
	 * check if a xml attribute is empty
	 * 
	 * @param value
	 * @return
	 */
	public static final boolean isAttrEmpty(String value) {
		if (isEmpty(value)) {
			return true;
		}
		return value.equalsIgnoreCase("N/A");
	}

	/**
	 * tries to parse a xml date value
	 * 
	 * @param value
	 * @return Date
	 */
	public static final Date parseXmlDate(String value) {
		Date date = null;
		if (value != null) {
			SimpleDateFormat sdf2 = new SimpleDateFormat(DATE_PATTERN_XML, LOCALE_XML);
			try {
				date = sdf2.parse(value);
			} catch (ParseException e) {
				System.err.println("Unable to parse as date: " + value);
				e.printStackTrace();
			}
		}
		return date;
	}
}
