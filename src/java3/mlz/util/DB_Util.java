package java3.mlz.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Database service utilities
 * 
 * @author retoweber
 *
 */
public final class DB_Util {
	private static final String DATA_SOURCE_NAME = "jdbc/simplePool";

	private static Connection getDataSource() throws Exception {
		try {
			InitialContext context = new InitialContext();
			DataSource ds = (DataSource) context.lookup(DATA_SOURCE_NAME);
			Connection conn = ds.getConnection();
			return conn;
		} catch (Exception e) {
			System.err.println("Cannot access initialContext for getting a datasource: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * returns a preperatedStatement
	 * 
	 * @param sql
	 * @return PerparedStatment
	 */
	public static final PreparedStatement getPreparedStatement(String sql) throws SQLException {
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
			conn = getDataSource();
			pstm = conn.prepareStatement(sql);
		} catch (SQLException e) {
			System.err.println("Error: Unable to create a database statement with: " + sql + " \n" + e.getMessage());
			cleanUpConnection(conn);
			throw e;
		} catch (Exception e) {
			cleanUpConnection(conn);
			throw new SQLException(e.getMessage());
		}
		return pstm;
	}

	/**
	 * executes a prepared sql query statement and returns a result set
	 * 
	 * @param pstm
	 * @return ResultSet of the query
	 * @throws SQLException
	 */
	public static final ResultSet executeQuery(PreparedStatement pstm) throws SQLException {
		ResultSet rs = null;
		try {
			rs = pstm.executeQuery();
		} catch (SQLException e) {
			System.err.println("Error: Unable execute sql query: \n" + e.getMessage());
			cleanUpResultSet(rs);
			cleanUpStatment(pstm);
			throw e;
		}
		return rs;
	}

	/**
	 * executes a prepared update statement and returns the number of affected rows
	 * 
	 * @param pstm
	 * @return number of affected rows
	 * @throws SQLException
	 */
	public static final int executeUpdate(PreparedStatement pstm) throws SQLException {
		int rows = 0;
		try {
			rows = pstm.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Error: Unable execute update: \n" + e.getMessage());
			throw e;
		} finally {
			cleanUpStatment(pstm);
		}
		return rows;
	}

	/**
	 * clean up result sets in finally
	 * 
	 * @param rs
	 */
	public static final void cleanUpResultSet(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException err) {
				System.out.println("Warning: Unable to close the recordset: \n" + err.getMessage());
			}
		}
	}

	/**
	 * clean up for database statements in finally
	 * 
	 * @param stm
	 */
	public static final void cleanUpStatment(Statement stm) {
		Connection conn = null;
		if (stm != null) {
			try {
				conn = stm.getConnection();
			} catch (SQLException ignore) {
				// try to get the connection
			}
			try {
				stm.close();
			} catch (SQLException err) {
				System.out.println("Warning: Unable to close database statment: \n" + err.getMessage());
			}
			cleanUpConnection(conn);
		}
	}

	/**
	 * clean up for database connection in finally
	 * 
	 * @param conn
	 */
	public static final void cleanUpConnection(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException err) {
				System.out.println("Warning: Unable to close database connection: \n" + err.getMessage());
			}
		}
	}
}
