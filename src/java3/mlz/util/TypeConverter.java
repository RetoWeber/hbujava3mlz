package java3.mlz.util;

/**
 * Class for data type conversions (all on one place, potential reusable)
 * 
 * @author retoweber
 *
 */
public final class TypeConverter {
	private TypeConverter() {
		// not needed
	}

	/**
	 * converts an util date to a new sql date instance, null date becomes to
	 * initial "zero" SQL date
	 * 
	 * @param date
	 * @return java.sql.Date
	 */
	public static final java.sql.Date convertToSqlDate(java.util.Date date) {
		if (date == null)
			return null;
		return new java.sql.Date(date.getTime());
	}

}
