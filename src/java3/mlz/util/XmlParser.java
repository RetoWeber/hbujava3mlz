package java3.mlz.util;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

/**
 * Helper class to parse XMLs
 * 
 * @author retoweber
 *
 */
public final class XmlParser {
	private XmlParser() {
		// not needed
	}

	/**
	 * parse a xml inputStream with a handler as in/out parameter
	 * 
	 * @param handler
	 * @param is
	 */
	public static final void parseXML(DefaultHandler handler, InputStream is) {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();

			saxParser.parse(is, handler);

		} catch (ParserConfigurationException ex) {
			System.err.println("SAX Config Error: " + ex.getLocalizedMessage());
			ex.printStackTrace();
		} catch (IOException ex) {
			System.err.println("IO Error: " + ex.getLocalizedMessage());
			ex.printStackTrace();
		} catch (org.xml.sax.SAXException e) {
			System.err.println("SAX Error: " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

}
