package java3.mlz.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java3.mlz.model.Movie;
import java3.mlz.util.Value;

/**
 * class to parse the movie XML
 * 
 * @author retoweber
 *
 */
public final class XmlHandlerMovie extends DefaultHandler {
	private static final String XML_TAG = "movie";
	private List<Movie> movies = new ArrayList<Movie>();

	@Override
	public void startElement(String url, String localName, String qName, Attributes attrs) throws SAXException {
		// looking for open movie element and parse its attributes
		if (qName.equals(XML_TAG)) {
			parseMovieAttributes(attrs);
		}
	}

	private void parseMovieAttributes(Attributes attrs) {
		Movie movie = new Movie();
		movie.setActors(attrs.getValue("actors"));
		movie.setAwards(attrs.getValue("awards"));
		movie.setCountry(attrs.getValue("country"));
		movie.setDirector(attrs.getValue("director"));
		movie.setImdbID(attrs.getValue("imdbID"));
		// get only number and remove any thousand separators.
		String attr = attrs.getValue("imdbRating");
		movie.setImdbRating(Value.isAttrEmpty(attr) ? null : Float.valueOf(attr.replaceAll(",", "")));
		// get only number and remove any thousand separators.
		attr = attrs.getValue("imdbVotes");
		movie.setImdbVotes(Value.isAttrEmpty(attr) ? null : Long.valueOf(attr.replaceAll(",", "")));
		movie.setLanguage(attrs.getValue("language"));
		movie.setGenre(attrs.getValue("genre"));
		movie.setMetascore(attrs.getValue("metascore"));
		movie.setPlot(attrs.getValue("plot"));
		attr = attrs.getValue("poster");
		movie.setPoster(Value.isAttrEmpty(attr) ? "none" : attr);
		movie.setRated(attrs.getValue("rated"));
		// get only dates not any comments n/a...
		attr = attrs.getValue("released");
		Date released = Value.isAttrEmpty(attr) ? null : Value.parseXmlDate(attr);
		movie.setReleased(released);
		movie.setRuntime(attrs.getValue("runtime"));
		movie.setTitle(attrs.getValue("title"));
		movie.setType(attrs.getValue("type"));
		movie.setWriter(attrs.getValue("writer"));
		movie.setYear(attrs.getValue("year"));
		movies.add(movie);
	}

	/**
	 * getter of the parsed object
	 * 
	 * @return
	 */
	public final List<Movie> getMovies() {
		if (movies.size() == 0)
			return null;
		return movies;
	}

}
