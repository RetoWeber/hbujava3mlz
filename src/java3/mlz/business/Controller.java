package java3.mlz.business;

import java.io.InputStream;
import java.util.List;

import java3.mlz.model.Movie;
import java3.mlz.util.XmlParser;

/**
 * business logic controller
 * 
 * @author retoweber
 *
 */
public final class Controller {
	private Controller() {
		// not needed
	}

	private static final String PAGE_MY_MOVIES = "my_movies.jsf";

	/**
	 * load movies from database
	 * 
	 * @return List<Movie>
	 */
	public static final List<Movie> loadMovies() {
		return MovieDAO.getMovies();
	}

	/**
	 * load movies from database
	 * 
	 * @return List<Movie>
	 */
	public static final Movie loadMovie(int movie_id) {
		return MovieDAO.getMovie(movie_id);
	}

	/**
	 * delete movie and returns the string to navigate to the overview
	 * 
	 * @param movie
	 * @return link
	 */
	public static final String deleteMovie(Movie movie) {

		MovieDAO.deleteEntry(movie);

		return PAGE_MY_MOVIES;
	}

	/**
	 * save movie and returns the string to navigate to the overview
	 * 
	 * @param movie
	 * @return link
	 */
	public static final String saveMovie(Movie movie) {

		MovieDAO.insertEntry(movie);

		return PAGE_MY_MOVIES;
	}

	/**
	 * returns the string to the overview page
	 * 
	 * @return link
	 */
	public static final String goToMyMovies() {
		return PAGE_MY_MOVIES;
	}

	/**
	 * searches in OmDB and return a movie object if found
	 * 
	 * @param title
	 * @param year
	 * @return Movie
	 */
	public static final List<Movie> searchInOMDB(String title, Integer year) {
		MovieSearchDAO movieSearch = new MovieOMDBSearchDAO();
		InputStream is = movieSearch.findMovieAsStream(title, String.valueOf(year));
		XmlHandlerMovie handler = new XmlHandlerMovie();
		XmlParser.parseXML(handler, is);
		return handler.getMovies();
	}

}
