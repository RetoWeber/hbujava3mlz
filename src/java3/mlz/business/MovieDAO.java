package java3.mlz.business;

import static java3.mlz.util.TypeConverter.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java3.mlz.model.Movie;
import java3.mlz.util.DB_Util;

/**
 * Database access object class for movies
 * 
 * @author retoweber
 *
 */
public final class MovieDAO {
	private static final String TABLE_NAME = " movies_reto ";
	private static final String SQL_DELETE = "Delete from" + TABLE_NAME + "Where movie_id = ?";
	private static final String SQL_INSERT = "Insert into" + TABLE_NAME
			+ "(title, year, rated, released, runtime, genre, director, writer, actors, plot, language, country, awards, poster, metascore, imdb_rating, imdb_votes, imdb_id, type) Values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String SQL_SELECT_ALL = "Select * From" + TABLE_NAME + "order by title, year";
	private static final String SQL_SELECT_ONE = "Select * From" + TABLE_NAME + "Where movie_id = ?";

	/**
	 * deletes a movie record
	 * 
	 * @param movie
	 */
	public static final void deleteEntry(Movie movie) {
		if (movie != null) {
			PreparedStatement pstm = null;
			try {
				pstm = DB_Util.getPreparedStatement(SQL_DELETE);
				pstm.setInt(1, movie.getId());
				DB_Util.executeUpdate(pstm);
			} catch (SQLException ex) {
				System.err.println("Error while deleting movie " + movie.getTitle() + ": " + ex.getLocalizedMessage());
			} finally {
				DB_Util.cleanUpStatment(pstm);
			}
		}
	}

	/**
	 * inserts a movie record
	 * 
	 * @param movie
	 */
	public static final void insertEntry(Movie movie) {
		if (movie != null) {
			PreparedStatement pstm = null;
			try {
				pstm = DB_Util.getPreparedStatement(SQL_INSERT);
				pstm.setString(1, movie.getTitle());
				pstm.setString(2, movie.getYear());
				pstm.setString(3, movie.getRated());
				pstm.setDate(4, convertToSqlDate(movie.getReleased()));
				pstm.setString(5, movie.getRuntime());
				pstm.setString(6, movie.getGenre());
				pstm.setString(7, movie.getDirector());
				pstm.setString(8, movie.getWriter());
				pstm.setString(9, movie.getActors());
				pstm.setString(10, movie.getPlot());
				pstm.setString(11, movie.getLanguage());
				pstm.setString(12, movie.getCountry());
				pstm.setString(13, movie.getAwards());
				pstm.setString(14, movie.getPoster());
				pstm.setString(15, movie.getMetascore());
				pstm.setFloat(16, (movie.getImdbRating() != null) ? movie.getImdbRating() : 0f);
				pstm.setLong(17, (movie.getImdbVotes() != null) ? movie.getImdbVotes() : 0);
				pstm.setString(18, movie.getImdbID());
				pstm.setString(19, movie.getType());
				DB_Util.executeUpdate(pstm);
			} catch (SQLException ex) {
				System.err.println("Error while Inserting movie " + movie.getTitle() + ": " + ex.getLocalizedMessage());
			} finally {
				DB_Util.cleanUpStatment(pstm);
			}
		}
	}

	/**
	 * reads a movie
	 * 
	 * @return List<Movies>
	 */
	public static final Movie getMovie(int movie_id) {
		Movie movie = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			pstm = DB_Util.getPreparedStatement(SQL_SELECT_ONE);
			pstm.setInt(1, movie_id);
			rs = DB_Util.executeQuery(pstm);
			while (rs.next()) {
				movie = readAttributes(rs);
			}
		} catch (SQLException ex) {
			System.err.println("Error while reading movie: " + ex.getLocalizedMessage());
		} finally {
			DB_Util.cleanUpResultSet(rs);
			DB_Util.cleanUpStatment(pstm);
		}
		return movie;
	}

	/**
	 * reads all movies
	 * 
	 * @return List<Movies>
	 */
	public static final List<Movie> getMovies() {
		List<Movie> movies = new ArrayList<Movie>();
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			pstm = DB_Util.getPreparedStatement(SQL_SELECT_ALL);
			rs = DB_Util.executeQuery(pstm);
			while (rs.next()) {
				Movie movie = readAttributes(rs);
				movies.add(movie);
			}
		} catch (SQLException ex) {
			System.err.println("Error while reading movies: " + ex.getLocalizedMessage());
		} finally {
			DB_Util.cleanUpResultSet(rs);
			DB_Util.cleanUpStatment(pstm);
		}
		return movies;
	}

	private static Movie readAttributes(ResultSet rs) throws SQLException {
		// id with object
		int id = rs.getInt("movie_id");
		Movie movie = new Movie(id);
		// attributes
		movie.setActors(rs.getString("actors"));
		movie.setAwards(rs.getString("awards"));
		movie.setCountry(rs.getString("country"));
		movie.setDirector(rs.getString("director"));
		movie.setGenre(rs.getString("genre"));
		movie.setImdbID(rs.getString("imdb_id"));
		movie.setImdbRating(rs.getFloat("imdb_Rating"));
		movie.setImdbVotes(rs.getLong("imdb_Votes"));
		movie.setLanguage(rs.getString("language"));
		movie.setMetascore(rs.getString("metascore"));
		movie.setPlot(rs.getString("plot"));
		movie.setPoster(rs.getString("poster"));
		movie.setRated(rs.getString("rated"));
		movie.setReleased(rs.getDate("released"));
		movie.setRuntime(rs.getString("runtime"));
		movie.setTitle(rs.getString("title"));
		movie.setType(rs.getString("type"));
		movie.setWriter(rs.getString("writer"));
		movie.setYear(rs.getString("year"));
		return movie;
	}

}
