package java3.mlz.business;

import java.io.InputStream;

/**
 * The interface offers two methods to search for movies
 * @author fean
 *
 */
public interface MovieSearchDAO {

	/**
	 * Searches a movie with the given search attributes and returns it as a XML string
	 * @param title movie title to search for
	 * @param year year of release
	 * @return the movie represented as a XML string
	 */
	String findMovieAsString(String title, String year);

	/**
	 * Searches a movie with the given search attributes and returns it represented as a stream (XML)
	 * @param title movie title to search for 
	 * @param year year of release
	 * @return the movie represented as a stream (XML)
	 */
	InputStream findMovieAsStream(String title, String year);

}