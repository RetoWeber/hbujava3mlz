package java3.mlz.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
/**
 * 
 * @author fean
 *
 */
public class MovieOMDBSearchDAO implements MovieSearchDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java3.mlz.business.MovieSearchDAO#findMovieAsString(java.lang.String,
	 * java.lang.Integer)
	 */
	@Override
	public String findMovieAsString(String title, String year) {

		InputStream input = findMovieAsStream(title, year);

		String xmlString = convertStreamToString(input);

		return xmlString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java3.mlz.business.MovieSearchDAO#findMovieAsStream(java.lang.String,
	 * java.lang.Integer)
	 */
	@Override
	public InputStream findMovieAsStream(String title, String year) {

		InputStream is = null;

		try {

			StringBuffer url = new StringBuffer();
			url.append("http://www.omdbapi.com/?apikey=51cd8af0");
			url.append("&t=" + URLEncoder.encode(title, "UTF-8"));
			if (year != null) {
				url.append("&y=");
				url.append(year);
			}
			url.append("&r=");
			url.append(URLEncoder.encode("xml", "UTF-8"));

			is = new URL(url.toString()).openStream();

		} catch (IOException e) {
			System.out.println(e);
		}

		return is;
	}

	private String convertStreamToString(java.io.InputStream is) {
		try (java.util.Scanner s = new java.util.Scanner(is)) {
			return s.useDelimiter("\\A").hasNext() ? s.next() : "";
		}
	}

	// main Methode just used as a playground to try out if and how the OMDBAPI works
	public static void main(String[] args) {
		MovieSearchDAO bo = new MovieOMDBSearchDAO();
		String xml = bo.findMovieAsString("any*", null);
		System.out.println(xml);

		InputStream xmlStream = bo.findMovieAsStream("any*", null);
		System.out.println(xmlStream);

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {
			System.out.println("Expected Stream length: " + xmlStream.available());

			br = new BufferedReader(new InputStreamReader(xmlStream));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("Stream content: " + sb.toString());

	}

}
