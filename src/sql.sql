-- table for saving the movie data, maximum possible votes are about 2.1E9 
create table movies_reto (
movie_id int not null primary key auto_increment,
title varchar(1000) not null,
year varchar(20),
rated varchar(100),
released date,
runtime varchar(20),
genre varchar(1000),
director varchar(1000),
writer varchar(1000),
actors varchar(1000),
plot varchar(2000),
language varchar(48),
country varchar(48),
awards varchar(1000),
poster varchar(1000),
metascore varchar(20),
imdb_rating float,
imdb_votes int,
imdb_id varchar(20),
type varchar(64) not null
);