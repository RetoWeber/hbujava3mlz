package test.java3.mlz.util;

import java.io.InputStream;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import java3.mlz.business.MovieSearchDAO;
import java3.mlz.business.XmlHandlerMovie;
import java3.mlz.model.Movie;
import java3.mlz.util.XmlParser;

/**
 * Junit class to test xmlHanderMovie
 * 
 * @author retoweber
 *
 */
public class TestMovieSearch {

	@Test
	public void testParseXML_1() {
		MovieSearchDAO movieSearch = new DummyMovieOMDBSearchDAO();
		InputStream is = movieSearch.findMovieAsStream("Test", "2000");
		XmlHandlerMovie movieHandler = new XmlHandlerMovie();
		XmlParser.parseXML(movieHandler, is);
		List<Movie> movies = movieHandler.getMovies();
		Movie movie = movies.get(0);
		String year = String.valueOf(movie.getYear());
		Assert.assertEquals("Year in movie", "2000", year);
		Assert.assertNotNull(movie.getTitle());
		Assert.assertNotNull(movie.getReleased());
	}

	@Test
	public void testParseXML_2() {
		MovieSearchDAO movieSearch = new DummyMovieOMDBSearchDAO();
		InputStream is = movieSearch.findMovieAsStream("Bond", "2004");
		XmlHandlerMovie movieHandler = new XmlHandlerMovie();
		XmlParser.parseXML(movieHandler, is);
		List<Movie> movies = movieHandler.getMovies();
		Movie movie = movies.get(0);
		String year = String.valueOf(movie.getYear());
		Assert.assertEquals("Year in movie", "2004", year);
		Assert.assertNull(movie.getReleased());
	}
}
