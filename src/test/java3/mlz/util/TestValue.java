/**
 * 
 */
package test.java3.mlz.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import java3.mlz.util.Value;

/**
 * JUNIT Class to test Value class
 * 
 * @author retoweber
 *
 */
public class TestValue {
	static final SimpleDateFormat sdfXML = new SimpleDateFormat(Value.DATE_PATTERN_XML, Value.LOCALE_XML);

	/**
	 * Test method for {@link java3.mlz.business.util.Value#isEmpty(java.lang.String)}.
	 */
	@Test
	public void testIsEmpty_Empty() {
		assertTrue("is Empty True", Value.isEmpty(""));
	}

	@Test
	public void testIsEmpty_Null() {
		assertTrue("is Empty Null", Value.isEmpty(null));
	}

	@Test
	public void testIsEmpty_NotEmpty() {
		assertFalse("is Empty Not", Value.isEmpty("Not Empty"));
	}

	/**
	 * Test method for
	 * {@link java3.mlz.business.util.Value#parseXmlDate(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testParseDateXML1990_12_22() {
		String dateValue = "22 Dec 1990";
		Date date = Value.parseXmlDate(dateValue);
		if (date == null) {
			fail("Date could be parsed");
		} else {
			String result = sdfXML.format(date);
			assertEquals("parse date " + dateValue, dateValue, result);
		}
	}

	@Test
	public void testParseDateXML1966_05_01() {
		String dateValue = "01 May 1966";
		Date date = Value.parseXmlDate(dateValue);
		if (date == null) {
			fail("Date could be parsed");
		} else {
			String result = sdfXML.format(date);
			assertEquals("parse date " + dateValue, dateValue, result);
		}
	}
	
	@Test
	public void testParseDateXML1966_05_1() {
		String dateValue = "1 May 1966";
		String expected = "01 May 1966";
		Date date = Value.parseXmlDate(dateValue);
		if (date == null) {
			fail("Date could be parsed");
		} else {
			String result = sdfXML.format(date);
			assertEquals("parse date " + dateValue, expected, result);
		}
	}

}
