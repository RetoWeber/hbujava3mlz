package test.java3.mlz.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import java3.mlz.business.MovieSearchDAO;

/**
 * Dummy class to test OMDB search with connection
 * 
 * @author retoweber
 *
 */
public class DummyMovieOMDBSearchDAO implements MovieSearchDAO {

	@Override
	public String findMovieAsString(String title, String year) {

		if (title.equalsIgnoreCase("bond") && year.equals("2004")) {
			return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root response=\"True\"><movie title=\"Rene Bond: Sex Kitten\" year=\"2004\" rated=\"X\" released=\"N/A\" runtime=\"120 min\" genre=\"Adult\" director=\"N/A\" writer=\"N/A\" actors=\"Rene Bond\" plot=\"N/A\" language=\"English\" country=\"USA\" awards=\"N/A\" poster=\"N/A\" metascore=\"N/A\" imdbRating=\"N/A\" imdbVotes=\"N/A\" imdbID=\"tt3727400\" type=\"movie\"/></root>";
		}
		if (title.equalsIgnoreCase("test") && year.equals("2000")) {
			return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root response=\"True\"><movie title=\"Test Film: Sex Kitten\" year=\"2000\" rated=\"X\" released=\"4 Dec 2000\" runtime=\"120 min\" genre=\"Adult\" director=\"N/A\" writer=\"N/A\" actors=\"Rene Bond\" plot=\"N/A\" language=\"English\" country=\"USA\" awards=\"N/A\" poster=\"N/A\" metascore=\"N/A\" imdbRating=\"N/A\" imdbVotes=\"N/A\" imdbID=\"tt3727400\" type=\"movie\"/></root>";
		}
		return "";
	}

	@Override
	public InputStream findMovieAsStream(String title, String year) {
		String xml = this.findMovieAsString(title, year);
		return new ByteArrayInputStream(xml.getBytes());
	}

}
