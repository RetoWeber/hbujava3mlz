package test.java3.mlz.util;

import java3.mlz.business.MovieOMDBSearchDAO;
import java3.mlz.business.MovieSearchDAO;

/**
 * Class to call a OMDB Search
 * 
 * @author retoweber
 *
 */
public class TestCaller_searchOMDB {

	public static void main(String[] args) {
		MovieSearchDAO movieSearch = new MovieOMDBSearchDAO();
		String xml = movieSearch.findMovieAsString("Bond", "2004");
		System.out.println("XML Result: \n" + xml);
	}

}
